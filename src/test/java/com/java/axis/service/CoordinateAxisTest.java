package com.java.axis.service;

import com.java.axis.entities.Circle;
import com.java.axis.entities.Point;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CoordinateAxisTest {

    CoordinateAxis coordinateAxis;
    Circle circle = new Circle(new Point(10, 10), 6);
    List<Point> points = new ArrayList<>();

    @BeforeAll
    public void addList() {
        points.add(new Point(1, 5));
        points.add(new Point(3, 7));
        points.add(new Point(2, 1));
        points.add(new Point(10, 5));
        points.add(new Point(7, 4));
        points.add(new Point(9, 5));
        points.add(new Point(3, 4));
        points.add(new Point(6, 11));
        points.add(new Point(12, 5));
        points.add(new Point(6, 4));
    }


    @BeforeEach
    public void setUp() {
        coordinateAxis = Mockito.mock(CoordinateAxis.class);
    }

    @Test
    public void shouldBeInitializationCircle() {
        Mockito.when(coordinateAxis.circleInitialization()).thenReturn(circle);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(coordinateAxis.circleInitialization().getRadius()).isEqualTo(6);
        softly.assertThat(coordinateAxis.circleInitialization().getPoint().getX()).isEqualTo(10);
        softly.assertThat(coordinateAxis.circleInitialization().getPoint().getY()).isEqualTo(10);
        softly.assertAll();
    }

    @Test
    public void shouldBeAddedFullList() {
        Mockito.when(coordinateAxis.addPoints()).thenReturn(points);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(coordinateAxis.addPoints().size()).isEqualTo(10);
        softly.assertThat(coordinateAxis.addPoints().get(0).getX()).isEqualTo(1);
        softly.assertThat(coordinateAxis.addPoints().get(0).getY()).isEqualTo(5);
        softly.assertAll();
    }

    @Test
    public void shouldBeCorrectFormula() {
        Mockito.when(coordinateAxis.circleInitialization()).thenReturn(circle);

        assertTrue(circle.containTo(points.get(3)));
    }
}