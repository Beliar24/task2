package com.java.axis;

import com.java.axis.service.CoordinateAxis;

import java.util.Scanner;

public class RunFormula {

    public static void main(String[] args) {
        CoordinateAxis coordinateAxis = new CoordinateAxis(new Scanner(System.in));
        coordinateAxis.isCoordinate(coordinateAxis.addPoints(), coordinateAxis.circleInitialization());
    }
}
