package com.java.axis.service;

import com.java.axis.entities.Circle;
import com.java.axis.entities.Point;
import lombok.Getter;

import java.util.*;

@Getter
public class CoordinateAxis {

    private final Scanner scanner;
    private final List<Point> points = new ArrayList<>();

    public CoordinateAxis(Scanner scanner) {
        this.scanner = scanner;
    }

    public void isCoordinate(List<Point> points, Circle circle) {
        for (Point point : points) {
            if(circle.containTo(point)) {
                System.out.println("The point " + point + " included into circle");
            }
        }
    }

    public List<Point> addPoints() {
        for (int i = 1; i < 11; i++) {
            System.out.println("Enter x and y for " + i + " point");
            points.add(new Point(scanner.nextInt(), scanner.nextInt()));
        }
        return points;
    }

    public Circle circleInitialization() {
        System.out.println("Enter centre of circle(x and y) and radius");
        return new Circle(new Point(scanner.nextInt(), scanner.nextInt()), scanner.nextInt());
    }
}