package com.java.axis.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Circle {

    private final Point point;
    private final int radius;

    private Point getCenter() {
        return point;
    }

    public double distanceTo(Point point) {
        return Math.sqrt(Math.pow(point.getX() - getCenter().getX(), 2) + Math.pow(point.getY() - getCenter().getY(), 2));
    }

    public boolean containTo(Point point) {
        boolean isContain = false;
        double result = distanceTo(point);
        if (result < getRadius()) {
            isContain = true;
        }
        return isContain;
    }
}
